/*  Copyright (C) 2008 National Institute For Space Research (INPE) - Brazil.

    This file is part of the TerraLib - a Framework for building GIS enabled applications.

    TerraLib is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License,
    or (at your option) any later version.

    TerraLib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with TerraLib. See COPYING. If not, write to
    TerraLib Team at <terralib-team@terralib.org>.
 */

/*!
  \file TsRasterSummaryManager.cpp
 
  \brief A test suit for the Raster Summary Manager class.
 */

#include "TsRasterSummaryManager.h"
#include "../Config.h"

#include <terralib/raster/RasterSummaryManager.h>

#include <boost/shared_ptr.hpp>

CPPUNIT_TEST_SUITE_REGISTRATION( TsRasterSummaryManager );

void TsRasterSummaryManager::tcRasterSummaryManagerConstructor()
{
}

void TsRasterSummaryManager::tcRasterSummaryManagerAdd()
{
}

void TsRasterSummaryManager::tcRasterSummaryManagerFind()
{
}

void TsRasterSummaryManager::tcRasterSummaryManagerRemove()
{
}

void TsRasterSummaryManager::tcRasterSummaryManagerGet()
{
}