## **Terralib 5** - CLI access to segmentation module

A modified/contributed version of Terralib-5 with CLI access to Baatz and Mean Region Growing image segmentation algorithms.

Provides binaries for Windows x64 built with Visual Studio.

Note of caution: all features are experimental!