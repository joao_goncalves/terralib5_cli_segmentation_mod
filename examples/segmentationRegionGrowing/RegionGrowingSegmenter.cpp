
#include "RegionGrowing.h"

// TerraLib
#include <terralib/raster.h>
#include <terralib/dataaccess.h>
#include <terralib/geometry.h>
#include <terralib/memory.h>
#include <terralib/rp.h>

// STL
#include <string>
#include <map>



void RegionGrowingSegmenter(std::string inRaster, 
							unsigned int fromRasterBand,
							unsigned int toRasterBand,
							double thresh,
							unsigned int minSize,
							std::string outRaster){

	try{


		// --------------------------------------------------------------------------------------- //
		// Open input raster

		std::cout << "-> Reading raster data for specified bands..." << std::endl;

		std::map<std::string, std::string> rinfo;

		rinfo["URI"] = inRaster;
		te::rst::Raster* rin = te::rst::RasterFactory::open(rinfo);


		// --------------------------------------------------------------------------------------- //
		// Define segmentation parameters

		te::rp::Segmenter::InputParameters algoInputParameters;
		algoInputParameters.m_inputRasterPtr = rin;

		//std::cout << "Loading raster data for specified bands..." << std::endl;

		// Push raster data for the band interval defined by the user
		//
		for (unsigned int i = fromRasterBand; i < toRasterBand; i++){
			algoInputParameters.m_inputRasterBands.push_back(i);
		}

		std::cout << "done.\n" << std::endl;

		algoInputParameters.m_strategyName = "RegionGrowingMean";

		//algoInputParameters.m_enableProgress = true;

		algoInputParameters.m_enableBlockProcessing = false;

		algoInputParameters.m_enableThreadedProcessing = false;

		algoInputParameters.m_enableRasterCache = true;


		// --------------------------------------------------------------------------------------- //
		// Link specific parameters with chosen implementation


		te::rp::SegmenterRegionGrowingMeanStrategy::Parameters segparameters;

		segparameters.m_segmentsSimilarityThreshold = thresh;

		segparameters.m_minSegmentSize = minSize;

		algoInputParameters.setSegStrategyParams(segparameters);

		/*
		Not defining the remaining parameters (assuming defaults) for this segmentation strategy:

		-> unsigned int m_segmentsSimIncreaseSteps; //!< The maximum number of steps to increment the similarity threshold value for the cases where no segment merge occurred - zero will disable segment similarity threshold increments - defaul: 2.
            
        -> bool m_enableLocalMutualBestFitting; //!< If enabled, a merge only occurs between two segments if the minimum dissimilarity criteria is best fulfilled mutually (default: false).
            
        -> bool m_enableSameIterationMerges; //!< If enabled, a merged segment could be merged with another within the same iteration (default:false).            
            
		*/

		// --------------------------------------------------------------------------------------- //
		// Output parameters

		te::rp::Segmenter::OutputParameters algoOutputParameters;

		// Set output raster
		std::map<std::string, std::string> orinfo;
		orinfo["URI"] = outRaster;

		algoOutputParameters.m_rInfo = orinfo;
		algoOutputParameters.m_rType = "GDAL";


		// --------------------------------------------------------------------------------------- //
		// Execute Baatz segmentation algorithm

		std::cout << "-> Performing image segmentation with mean region-growing algorithm... " << std::endl;
		te::rp::Segmenter seginstance;

		if (!seginstance.initialize(algoInputParameters)) throw;

		if (!seginstance.execute(algoOutputParameters)) throw;

		std::cout << "done.\n" << std::endl;


		// Clean up
		delete rin;

	}

	catch (const std::exception& e){
		std::cout << std::endl << "An exception has occurred while performing Baatz segmentation: " << e.what() << std::endl;
	}

	catch (...){
		std::cout << std::endl << "An unexpected exception has occurred while performing Baatz segmentation!" << std::endl;
	}

}
