#include "CommonExamples.h"

// STL
#include <iostream>

void CharEncodingConv()
{
  // Charsets definitions
  //te::common::CharEncoding fromCode = te::common::UTF8;
  //te::common::CharEncoding toCode = te::common::CP1252;

  //// Charset descriptions
  //std::cout << "- Description (fromCode): " << te::common::CharEncodingConv::getDescription(fromCode) << std::endl;
  //std::cout << "- Description (toCode): " << te::common::CharEncodingConv::getDescription(toCode) << std::endl;

  //// Create a charset converter and convert a string
  //te::common::CharEncodingConv converter(fromCode, toCode);

  //std::string input = "my_input_string";
  //std::string output = converter.conv(input);
}
