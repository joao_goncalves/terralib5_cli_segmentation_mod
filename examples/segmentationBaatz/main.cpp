
#include "Baatz.h"

// TerraLib
#include <terralib/raster.h>
#include <terralib/dataaccess.h>
#include <terralib/geometry.h>
#include <terralib/memory.h>
#include <terralib/rp.h>
#include <terralib/common.h>
#include <terralib/plugin.h>

// STL
#include <cstdlib>
#include <iostream>
#include <string>
#include <map>
#include <vector>


int main(int argc, char* argv[])
{	

	if(argc < 8){
		
		std::cout << 
		"\n\n## --- TerraLib-5 Baatz region-growing image segmentation algorithm --- ##\n\n"
		"ARGUMENTS:\n"
		"[1] Input file\n"
		"[2] Raster band interval: from band (e.g., 0)\n"
		"[3] Raster band interval: to band (e.g., 2)\n"
		"[4] Compactness Weight: Related to how close to a rectangle will be the output regions (0.0,..., 1.0)\n"
		"[5] Color Weight: The influence of spectral properties to compute if pixels are similar (0.0, ...,1.0)\n"
		"[6] Threshold parameter (0.0, ..., 1.0)\n"
		"[7] Size of the smallest segment (in pixels)\n"
		"[8] The output raster path\n\n"
		"EXAMPLE:\n "
		"terralib_cli_baatz input_raster.tif 0 3 0.5 0.8 0.05 200 out_raster_segm_baatz.tif\n\n"
		<< std::endl;
		
		
	}else{
		
		// Parse and typecast input parameters from the command line
		//
		std::string inRaster = (std::string)argv[1];

		unsigned int fromRasterBand = atoi(argv[2]);
		
		unsigned int toRasterBand = atoi(argv[3]);
		
		std::string comp(argv[4]);
		double compactness = std::stod(comp);
		
		std::string col(argv[5]);
		double color = std::stod(col);
		
		std::string thr(argv[6]);
		double thresh = std::stod(thr);
		
		unsigned int minSize = atoi(argv[7]);
		
		std::string outRaster = (std::string)argv[8];
		

		// Print input parameters:
		std::cout << "\n\n" << std::endl;
		std::cout << "### ------------------------------------------------------------------------------------------------------------ ###" << std::endl;
		std::cout << "### ---- Running TerraLib-5 Baatz region-growing image segmentation algorithm with the following parameters ---- ###"<< std::endl;
		std::cout << "### ------------------------------------------------------------------------------------------------------------ ###\n" << std::endl;
		std::cout << "Compactness weight = " << compactness << std::endl;
		std::cout << "Color weight = " << color << std::endl;
		std::cout << "Similarity threshold = " << thresh << std::endl;
		std::cout << "Minimum size = " << minSize << std::endl;
		std::cout << "No block processing" << std::endl;
		std::cout << "No multi-thread processing" << std::endl;
		std::cout << "Input raster cached\n" << std::endl;
		std::cout << "### ------------------------------------------------------------------------------------------------------------ ###\n\n" << std::endl;

		try{
			
			// --------------------------------------------------------------------------------------- //
			std::cout << "-> Initializing TerraLib..." << std::endl;

			TerraLib::getInstance().initialize();

			std::cout << "done.\n" << std::endl;


			// --------------------------------------------------------------------------------------- //
			std::cout << "-> Loading TerraLib modules..." << std::endl;

			LoadModules();

			std::cout << "done.\n" << std::endl;


			// --------------------------------------------------------------------------------------- //
			// Perform segmentation with a call to the wrapper function
			//
			BaatzSegmenter(inRaster, fromRasterBand, toRasterBand, compactness, color, thresh, minSize, outRaster);



			// --------------------------------------------------------------------------------------- //
			// Unloading plug-ins

			std::cout << "-> Unloading plug-ins..." << std::endl;

			te::plugin::PluginManager::getInstance().unloadAll();

			std::cout << "done.\n" << std::endl;



			// --------------------------------------------------------------------------------------- //
			// Unloading instance

			std::cout << "-> Unloading instance..." << std::endl;

			TerraLib::getInstance().finalize();

			std::cout << "done.\n" << std::endl;

		  }
		  
		  catch(const std::exception& e){
			
			std::cout << std::endl << "An exception has occurred while performing Baatz segmentation: " << e.what() << std::endl;

			return EXIT_FAILURE;
		  }
		  
		  catch(...){
			std::cout << std::endl << "An unexpected exception has occurred while performing Baatz segmentation!" << std::endl;

			return EXIT_FAILURE;
			
		  }

		  return EXIT_SUCCESS;
	}
}
