<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>te::qt::plugins::rp::ArithmeticOpAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/ArithmeticOpAction.cpp" line="40"/>
        <source>Arithmetic Operations...</source>
        <translation>Operações Aritméticas ...</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::ClassifierAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/ClassifierAction.cpp" line="40"/>
        <source>Classifier...</source>
        <translation>Classificador...</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::ClippingAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/ClippingAction.cpp" line="41"/>
        <location filename="../../../src/terralib/qt/plugins/rp/ClippingAction.cpp" line="44"/>
        <source>Clipping...</source>
        <translation>Recorte...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/ClippingAction.cpp" line="88"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/ClippingAction.cpp" line="88"/>
        <source>The layer selected is invalid or does not have an raster representation.</source>
        <translation>A camada selecionada é inválida ou não tem uma representação matricial (raster).</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::ColorTransformAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/ColorTransformAction.cpp" line="40"/>
        <source>Color Transform...</source>
        <translation>Transformação de Cores...</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::ComposeBandsAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/ComposeBandsAction.cpp" line="40"/>
        <source>Compose / Decompose Bands...</source>
        <translation>Compõe / Decompõe Bandas...</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::ContrastAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/ContrastAction.cpp" line="40"/>
        <source>Contrast...</source>
        <translation>Contraste...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/ContrastAction.cpp" line="83"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/ContrastAction.cpp" line="83"/>
        <source>The layer selected is invalid or does not have an raster representation.</source>
        <translation>A camada selecionada é inválida ou não tem uma representação matricial (raster).</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::FilterAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/FilterAction.cpp" line="40"/>
        <source>Filter...</source>
        <translation>Filtragem...</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::FusionAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/FusionAction.cpp" line="40"/>
        <source>Fusion...</source>
        <translation>Fusão...</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::MixtureModelAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/MixtureModelAction.cpp" line="40"/>
        <source>Mixture Model...</source>
        <translation>Modelo de Mistura...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/MixtureModelAction.cpp" line="83"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/MixtureModelAction.cpp" line="83"/>
        <source>The layer selected is invalid or does not have an raster representation.</source>
        <translation>A camada selecionada é inválida ou não tem uma representação matricial (raster).</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::MosaicAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/MosaicAction.cpp" line="40"/>
        <source>Mosaic...</source>
        <translation>Mosiaco...</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::RasterizationAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/RasterizationAction.cpp" line="31"/>
        <source>Rasterization...</source>
        <translation>Rasterização...</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::RegisterAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/RegisterAction.cpp" line="39"/>
        <source>Register...</source>
        <translation>Registro...</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::SegmenterAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/SegmenterAction.cpp" line="41"/>
        <source>Segmenter...</source>
        <translation>Segmentação...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/SegmenterAction.cpp" line="84"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/SegmenterAction.cpp" line="84"/>
        <source>The layer selected is invalid or does not have an raster representation.</source>
        <translation>A camada selecionada é inválida ou não tem uma representação matricial (raster).</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::rp::VectorizationAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/rp/VectorizationAction.cpp" line="29"/>
        <source>Vectorization...</source>
        <translation>Vetorização...</translation>
    </message>
</context>
</TS>
