<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>AggregationDialogForm</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="14"/>
        <source>Aggregation Operation</source>
        <translation>Operação de Agregação</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="50"/>
        <source>Agreggates objects of a layer based on specified attributes</source>
        <translation>Objetos agregados de uma camada com base em atributos especificados</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="69"/>
        <source>Imagem</source>
        <translation>Imagem</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="83"/>
        <source>Input layer and the group attribute(s)</source>
        <translation>Camada de entrada e grupo de atributo(s)</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="110"/>
        <source>Filter:</source>
        <translation>Filtragem:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="124"/>
        <source>Only Selected</source>
        <translation>Somente Selecionados</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="131"/>
        <source>Calculate Statistics</source>
        <translation>Cálculos Estatísticos</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="164"/>
        <source>Output</source>
        <translation>Saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="170"/>
        <source>Repository:</source>
        <translation>Repositório:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="190"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="203"/>
        <source>BD</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="210"/>
        <source>Layer Name:</source>
        <translation>Nome da Camada:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="229"/>
        <source>Output Statistics</source>
        <translation>Estatísticas de Saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="239"/>
        <source>Select all:</source>
        <translation>Selecionar todas:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="246"/>
        <source>Reject all:</source>
        <translation>Rejeitar todas:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="304"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="324"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/AggregationDialogForm.ui" line="331"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>BufferDialogForm</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="14"/>
        <source>Buffer Operation</source>
        <translation>Operação de áreas de influências (Buffer)</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="55"/>
        <source>Imagem</source>
        <translation>Imagem</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="69"/>
        <source>Creates buffer areas to a specified distance around objects</source>
        <translation>Cria áreas de influência para uma determinada distância em torno de objetos</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="79"/>
        <source>Input Layer</source>
        <translation>Camada de Entrada</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="91"/>
        <source>Only Selected</source>
        <translation>Somente Selecionados</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="101"/>
        <source>Distance</source>
        <translation>Distância</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="107"/>
        <source>Fixed at</source>
        <translation>Fixado em</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="120"/>
        <source>The buffer distance should be given in  layer&apos; SRS unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="127"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="137"/>
        <source>This option is not implemented yet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="140"/>
        <source>From an attribute</source>
        <translation>De um atributo</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="191"/>
        <source>Rule for polygons</source>
        <translation>Regras para polígonos</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="205"/>
        <source>Inside and Outside</source>
        <translation>Dentro e Fora</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="230"/>
        <source>Image for Rule</source>
        <translation>Imagem para regra</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="237"/>
        <source>Only Outside</source>
        <translation>Somente Fora</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="260"/>
        <source>Only Inside</source>
        <translation>Somente Dentro</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="273"/>
        <source>Boundaries between buffers</source>
        <translation>Fronteiras entre as áreas de influências</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="306"/>
        <source>Dissolve</source>
        <translation>Dissolver</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="356"/>
        <source>Not Dissolve</source>
        <translation>Não Dissolver</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="366"/>
        <source>Copy input columns</source>
        <translation>Copiar colunas de entrada</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="377"/>
        <source>Multiple Levels</source>
        <translation>Múltiplos Níveis</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="398"/>
        <source>Number of levels:</source>
        <translation>Número de níveis:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="419"/>
        <source>Output</source>
        <translation>Saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="468"/>
        <source>Layer Name:</source>
        <translation>Nome da Camada:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="454"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="428"/>
        <source>Repository:</source>
        <translation>Repositório</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="441"/>
        <source>BD</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="502"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="509"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/BufferDialogForm.ui" line="516"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>GeometricOpOutputWizardPageForm</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="14"/>
        <source>WizardPage</source>
        <translation>Assistente de página</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="31"/>
        <source>Operation</source>
        <translation>Operação</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="40"/>
        <source>Convex Hull</source>
        <translation>Limite Convexo (Convex Hull)</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="50"/>
        <source>Centroid</source>
        <translation>Centróide</translation>
    </message>
    <message>
        <source>Minimum Bounding Rectangle (MBR)</source>
        <translation type="vanished">Mínimo Retângulo Envolvente (MBR)</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="60"/>
        <source>Minimum Bounding Rectangle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="70"/>
        <source>Area</source>
        <translation>Área</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="80"/>
        <source>Line</source>
        <translation>Linha</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="90"/>
        <source>Perimeter</source>
        <translation>Perímetro</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="102"/>
        <source>Methodology</source>
        <translation>Metodologia</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="108"/>
        <source>All Objects</source>
        <translation>Todos Objetos</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="118"/>
        <source>Aggregated Objetcts</source>
        <translation>Objetos Agregados</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="125"/>
        <source>By Attributes</source>
        <translation>Por Atributos</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="154"/>
        <source>Output</source>
        <translation>Saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="163"/>
        <source>Repository:</source>
        <translation>Repositório:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="183"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="196"/>
        <source>BD</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpOutputWizardPageForm.ui" line="203"/>
        <source>Layer Name:</source>
        <translation>Nome da Camada:</translation>
    </message>
</context>
<context>
    <name>GeometricOpWizardPageForm</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/GeometricOpWizardPageForm.ui" line="14"/>
        <source>Geometric Operation</source>
        <translation>Operação Geométrica</translation>
    </message>
</context>
<context>
    <name>IntersectionDialogForm</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="14"/>
        <source>Intersection Operation</source>
        <translation>Operação de Interseção</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="20"/>
        <source>Attribute Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="30"/>
        <source>Overlay Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="52"/>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="138"/>
        <source>Only Selected Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="87"/>
        <source>Computes a geometric intersection of the objects in two distinct layers</source>
        <translation>Calcula uma intersecção geométrica dos objectos de duas camadas distintas</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="106"/>
        <source>Imagem</source>
        <translation>Imagem</translation>
    </message>
    <message>
        <source>Input</source>
        <translation type="vanished">Entrada</translation>
    </message>
    <message>
        <source>Only Selected</source>
        <translation type="vanished">Somente Selecionados</translation>
    </message>
    <message>
        <source>Input layer</source>
        <translation type="vanished">Camada de entrada</translation>
    </message>
    <message>
        <source>Overlay layer</source>
        <translation type="vanished">Camada de saída</translation>
    </message>
    <message>
        <source>Include attributes of input layer</source>
        <translation type="vanished">Inclui atributos da camada de entrada</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="116"/>
        <source>Input Layer</source>
        <translation type="unfinished">Camada de Entrada</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="156"/>
        <source>Output</source>
        <translation>Saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="162"/>
        <source>Repository:</source>
        <translation>Repositório:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="172"/>
        <source>Layer Name:</source>
        <translation>Nome da Camada:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="185"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="201"/>
        <source>BD</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="220"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="240"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/IntersectionDialogForm.ui" line="247"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>LineToPolygonDialogForm</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="14"/>
        <source>Line to Polygon Operation</source>
        <translation>Linha para Operação de Polígono</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="50"/>
        <source>Generates a layer of polygons from a layer of lines.</source>
        <translation>Gera uma camada de polígonos a partir de uma camada de linhas.</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="69"/>
        <source>Imagem</source>
        <translation>Imagem</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="87"/>
        <source>Output layer of polygon(s)</source>
        <translation>Camada de polígono(s) de saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="93"/>
        <source>Repository:</source>
        <translation>Repositório:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="113"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="126"/>
        <source>BD</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="133"/>
        <source>Layer Name:</source>
        <translation>Nome da Camada:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="150"/>
        <source>Input layer of line(s)</source>
        <translation>Camada de Linha(s) de Entrada</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="169"/>
        <source>Only Selected</source>
        <translation>Somente Selecionados</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="190"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="210"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/LineToPolygonDialogForm.ui" line="217"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>MultipartToSinglepartDialogForm</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="14"/>
        <source>Multipart To Singlepart</source>
        <translation>Multi partes para Única parte</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="48"/>
        <source>Split multpart geometries in singlepart geometries</source>
        <translation>Geometrias multi partes em geometrias de uma única parte</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="67"/>
        <source>Imagem</source>
        <translation>Imagem</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="77"/>
        <source>Input layer</source>
        <translation>Camada de Entrada</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="91"/>
        <source>Only Selected</source>
        <translation>Somente Selecionadas</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="122"/>
        <source>Output</source>
        <translation>Saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="128"/>
        <source>Repository:</source>
        <translation>Repositório:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="148"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="161"/>
        <source>BD</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="168"/>
        <source>Layer Name:</source>
        <translation>Nome da Camada:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="196"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="216"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/MultipartToSinglepartDialogForm.ui" line="223"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>PolygonToLineDialogForm</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="14"/>
        <source>Polygon to Line Operation</source>
        <translation>Operação de Polígono para Linha</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="69"/>
        <source>Imagem</source>
        <translation>Imagem</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="169"/>
        <source>Only Selected</source>
        <translation>Somente Selecionados</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="126"/>
        <source>BD</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="93"/>
        <source>Repository:</source>
        <translation>Repositório:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="50"/>
        <source>Generates a layer of lines from a layer of polygons.</source>
        <translation>Gera uma camada de linhas a partir de uma camada de polígonos.</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="87"/>
        <source>Output layer of line(s)</source>
        <translation>Camada de linha(s) de saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="113"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="133"/>
        <source>Layer Name:</source>
        <translation>Nome da Camada:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="150"/>
        <source>Input layer of polygon(s)</source>
        <translation>Camada de polígono(s) de entrada</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="190"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="210"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/PolygonToLineDialogForm.ui" line="217"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>SummarizationDialogForm</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="14"/>
        <source>Summarization Operation</source>
        <translation>Operação de Sumarização</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="55"/>
        <source>Imagem</source>
        <translation>Imagem</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="69"/>
        <source>The summarization operation is applied by two layers</source>
        <translation>A operação de sumarização é aplicada sobre duas camadas</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="85"/>
        <source>Input layer to assign data to</source>
        <translation>Camada de entrada para atribuir dados para</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="94"/>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="187"/>
        <source>Only Selected</source>
        <translation>Somente Selecionados</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="110"/>
        <source>Output Attributes</source>
        <translation>Atributos de Saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="120"/>
        <source>Select all:</source>
        <translation>Selecionar todos:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="127"/>
        <source>Reject all:</source>
        <translation>Rejeitar todos:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="178"/>
        <source>Input layer to assign data from</source>
        <translation>Camada de entrada para atribuir dados de</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="203"/>
        <source>Spatial Relationship</source>
        <translation>Relacionamento Espacial</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="209"/>
        <source>Inside</source>
        <translation>Dentro</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="216"/>
        <source>Inside or Covered by</source>
        <translation>Dentro ou Coberto por</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="223"/>
        <source>Intersects</source>
        <translation>Interseção</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="230"/>
        <source>Crosses</source>
        <translation>Cruza</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="251"/>
        <source>Advanced</source>
        <translation>Avançado</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="266"/>
        <source>operation with all layers in memory</source>
        <translation>operação com todas as camadas em memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="269"/>
        <source>Whole operation in memory</source>
        <translation>Operação inteira na memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="279"/>
        <source>operation with only one layer in memory</source>
        <translation>operação com uma só camada em memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="282"/>
        <source>Partially operation in memory</source>
        <translation>Operação parcialmente em memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="289"/>
        <source>operation without memory use</source>
        <translation>operação sem fazer uso de memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="292"/>
        <source>Low memory use</source>
        <translation>Baixo uso de memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="308"/>
        <source>Output</source>
        <translation>Saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="323"/>
        <source>BD</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="330"/>
        <source>Repository:</source>
        <translation>Repositório:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="343"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="357"/>
        <source>Layer Name:</source>
        <translation>Nome da Camada:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="378"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="398"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/SummarizationDialogForm.ui" line="405"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>TransformationDialogForm</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="14"/>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="50"/>
        <source>Transformation of a vector layer</source>
        <translation>Transformação de uma camada vetorial</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="69"/>
        <source>Imagem</source>
        <translation>Imagem</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="87"/>
        <source>Transformation</source>
        <translation>Transformação</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="93"/>
        <source>Multi* -&gt; Single*</source>
        <translation>Multi* -&gt; Única*</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="116"/>
        <source>Single* -&gt; Multi*</source>
        <translation>Única* -&gt; Multi*</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="139"/>
        <source>Input layer and the group attribute(s)</source>
        <translation>Camada de entrada e grupo de atributo(s)</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="155"/>
        <source>Only Selected</source>
        <translation>Somente Selecionados</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="164"/>
        <source>Filter:</source>
        <translation>Filtragem:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="185"/>
        <source>Advanced</source>
        <translation>Avançada</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="200"/>
        <source>operation with all layers in memory</source>
        <translation>operação com todas as camadas em memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="203"/>
        <source>Whole operation in memory</source>
        <translation>Operação inteira na memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="213"/>
        <source>operation with only one layer in memory</source>
        <translation>operação com uma só camada em memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="216"/>
        <source>Partially operation in memory</source>
        <translation>Operação parcialmente em memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="223"/>
        <source>operation without memory use</source>
        <translation>operação sem fazer uso de memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="226"/>
        <source>Low memory use</source>
        <translation>Baixo uso de memória</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="242"/>
        <source>Output</source>
        <translation>Saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="257"/>
        <source>BD</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="264"/>
        <source>Repository:</source>
        <translation>Repositório:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="277"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="291"/>
        <source>Layer Name:</source>
        <translation>Nome da Camada:</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="312"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="332"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/ui/TransformationDialogForm.ui" line="339"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>te::vp::AggregationDialog</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/AggregationDialog.cpp" line="580"/>
        <source>Save as...</source>
        <translation>Salvar como...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/AggregationDialog.cpp" line="581"/>
        <source>Shapefile (*.shp *.SHP);;</source>
        <translation>Shapefile (*.shp *.SHP);;</translation>
    </message>
</context>
<context>
    <name>te::vp::BufferDialog</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/BufferDialog.cpp" line="325"/>
        <source>Save as...</source>
        <translation>Salavar como...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/BufferDialog.cpp" line="326"/>
        <source>Shapefile (*.shp *.SHP);;</source>
        <translation>Shapefile (*.shp *.SHP);;</translation>
    </message>
</context>
<context>
    <name>te::vp::GeometricOpOutputWizardPage</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/GeometricOpOutputWizardPage.cpp" line="59"/>
        <source>Output Layer Attributes</source>
        <translation>Atributos da camada de saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/GeometricOpOutputWizardPage.cpp" line="60"/>
        <source>Choose the output parameters that compose the output layer.</source>
        <translation>Escolha os parâmetros de saída que compreendem a camada de saída.</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/GeometricOpOutputWizardPage.cpp" line="233"/>
        <source>Open Directory</source>
        <translation>Abrir Diretório</translation>
    </message>
</context>
<context>
    <name>te::vp::GeometricOpWizard</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/GeometricOpWizard.cpp" line="75"/>
        <source>Geometric Operation</source>
        <translation>Operação Geométrica</translation>
    </message>
</context>
<context>
    <name>te::vp::GeometricOpWizardPage</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/GeometricOpWizardPage.cpp" line="59"/>
        <source>Output Layer Attributes</source>
        <translation>Atributos da Camada de Saída</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/GeometricOpWizardPage.cpp" line="60"/>
        <source>Choose the attributes that compose the output layer.</source>
        <translation>Escolha os parâmetros de saída que compreendem a camada de saída.</translation>
    </message>
</context>
<context>
    <name>te::vp::IntersectionDialog</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/IntersectionDialog.cpp" line="652"/>
        <source>Save as...</source>
        <translation>Salvar como...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/IntersectionDialog.cpp" line="653"/>
        <source>Shapefile (*.shp *.SHP);;</source>
        <translation>Shapefile (*.shp *.SHP);;</translation>
    </message>
</context>
<context>
    <name>te::vp::LineToPolygonDialog</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/LineToPolygonDialog.cpp" line="175"/>
        <source>Save as...</source>
        <translation>Salvar como...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/LineToPolygonDialog.cpp" line="176"/>
        <source>Shapefile (*.shp *.SHP);;</source>
        <translation>Shapefile (*.shp *.SHP);;</translation>
    </message>
</context>
<context>
    <name>te::vp::MultipartToSinglepartDialog</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="158"/>
        <source>Save as...</source>
        <translation>Salvar como...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="159"/>
        <source>Shapefile (*.shp *.SHP);;</source>
        <translation>Shapefile (*.shp *.SHP);;</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="178"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="186"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="197"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="205"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="213"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="219"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="237"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="253"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="270"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="300"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="306"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="323"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="340"/>
        <source>Multipart To Singlepart</source>
        <translation>Multi parte para Única parte</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="178"/>
        <source>Select an input layer.</source>
        <translation>Selecione uma camada de entrada.</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="186"/>
        <source>Can not execute this operation on this type of layer.</source>
        <translation>Não é possível executar esta operação neste tipo de camada.</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="197"/>
        <source>None selected object!</source>
        <translation>Nenhum objeto selecionado !</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="205"/>
        <source>The selected input data source can not be accessed.</source>
        <translation>A fonte de dados de entrada selecionadoa não pode ser acessada.</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="213"/>
        <source>Define a repository for the result.</source>
        <translation>Definir um repositório para o resultado.</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="270"/>
        <location filename="../../../src/terralib/vp/qt/MultipartToSinglepartDialog.cpp" line="323"/>
        <source>Error: could not operate.</source>
        <translation>Erro: não pode operar.</translation>
    </message>
</context>
<context>
    <name>te::vp::PolygonToLineDialog</name>
    <message>
        <location filename="../../../src/terralib/vp/qt/PolygonToLineDialog.cpp" line="176"/>
        <source>Save as...</source>
        <translation>Salvar como...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/vp/qt/PolygonToLineDialog.cpp" line="177"/>
        <source>Shapefile (*.shp *.SHP);;</source>
        <translation>Shapefile (*.shp *.SHP);;</translation>
    </message>
</context>
</TS>
