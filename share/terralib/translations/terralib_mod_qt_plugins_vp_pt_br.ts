<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>te::qt::plugins::vp::AggregationAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/AggregationAction.cpp" line="42"/>
        <source>Aggregation...</source>
        <translation>Agregação...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/AggregationAction.cpp" line="65"/>
        <source>Aggregation Result</source>
        <translation>Resultado da Agregação</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/AggregationAction.cpp" line="65"/>
        <source>The operation was concluded successfully. Would you like to add the layer to the project?</source>
        <translation>A operação foi concluída com êxito. Você gostaria de adicionar a camada para o projeto ?</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::vp::BufferAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/BufferAction.cpp" line="42"/>
        <source>Buffer...</source>
        <translation>Áreas de Influências (Buffer)</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/BufferAction.cpp" line="66"/>
        <source>Buffer Result</source>
        <translation>Resultado da Área de Influência (buffer)</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/BufferAction.cpp" line="66"/>
        <source>The operation was concluded successfully. Would you like to add the layer to the project?</source>
        <translation>A operação foi concluída com êxito. Você gostaria de adicionar a camada para o projeto ?</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::vp::GeometricOpAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/GeometricOpAction.cpp" line="42"/>
        <source>Geometric Operation...</source>
        <translation>Operação Geométrica...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/GeometricOpAction.cpp" line="66"/>
        <source>Geometric Operation</source>
        <translation>Operação Geométrica</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/GeometricOpAction.cpp" line="66"/>
        <source>The operation was concluded successfully. Would you like to add the layer to the project?</source>
        <translation>A operação foi concluída com êxito. Você gostaria de adicionar a camada para o projeto ?</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::vp::IntersectionAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/IntersectionAction.cpp" line="43"/>
        <source>Intersection...</source>
        <translation>Interseção...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/IntersectionAction.cpp" line="67"/>
        <source>Intersection Result</source>
        <translation>Resultado da Interseção</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/IntersectionAction.cpp" line="67"/>
        <source>The operation was concluded successfully. Would you like to add the layer to the project?</source>
        <translation>A operação foi concluída com êxito. Você gostaria de adicionar a camada para o projeto ?</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::vp::LineToPolygonAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/LineToPolygonAction.cpp" line="42"/>
        <source>Line to Polygon...</source>
        <translation>Linha para Polígono</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/LineToPolygonAction.cpp" line="66"/>
        <source>Line to Polygon Result</source>
        <translation>Resultado de Linha para Polígono</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/LineToPolygonAction.cpp" line="66"/>
        <source>The operation was concluded successfully. Would you like to add the layer to the project?</source>
        <translation>A operação foi concluída com êxito. Você gostaria de adicionar a camada para o projeto ?</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::vp::MultipartToSinglepartAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/MultipartToSinglepartAction.cpp" line="42"/>
        <source>Multipart To Singlepart...</source>
        <translation>Multi Feição para Única Feição...</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/MultipartToSinglepartAction.cpp" line="65"/>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/MultipartToSinglepartAction.cpp" line="65"/>
        <source>The operation was concluded successfully. Would you like to add the layer to the project?</source>
        <translation>A operação foi concluída com êxito. Você gostaria de adicionar a camada para o projeto ?</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::vp::PolygonToLineAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/PolygonToLineAction.cpp" line="42"/>
        <source>Polygon to Line...</source>
        <translation>Polígono para Linha</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/PolygonToLineAction.cpp" line="65"/>
        <source>Polygon to Line Result</source>
        <translation>Resultado da Polígono para Linha</translation>
    </message>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/PolygonToLineAction.cpp" line="65"/>
        <source>The operation was concluded successfully. Would you like to add the layer to the project?</source>
        <translation>A operação foi concluída com êxito. Você gostaria de adicionar a camada para o projeto ?</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::vp::SummarizationAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/SummarizationAction.cpp" line="40"/>
        <source>Summarization...</source>
        <translation>Sumarizaçãao...</translation>
    </message>
</context>
<context>
    <name>te::qt::plugins::vp::TransformationAction</name>
    <message>
        <location filename="../../../src/terralib/qt/plugins/vp/TransformationAction.cpp" line="40"/>
        <source>Transformation...</source>
        <translation>Transformação...</translation>
    </message>
</context>
</TS>
