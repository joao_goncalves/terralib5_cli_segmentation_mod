<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../../src/terraview/AboutDialog.cpp" line="45"/>
        <source>&lt;p&gt;Copyright &amp;copy; 2010-2015 INPE&lt;BR&gt;</source>
        <translation>&lt;p&gt;Copyright &amp;copy; 2010-2015 INPE&lt;BR&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/AboutDialog.cpp" line="48"/>
        <source>TerraView Version: </source>
        <translation>Versão do TerraView:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/AboutDialog.cpp" line="51"/>
        <source>TerraLib Version: </source>
        <translation>Versão da TerraLib:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/AboutDialog.cpp" line="54"/>
        <source>Build Date: </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AboutDialogForm</name>
    <message>
        <location filename="../../../src/terraview/ui/AboutDialogForm.ui" line="14"/>
        <source>About TerraView</source>
        <translation>Sobre o TerraView</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ui/AboutDialogForm.ui" line="81"/>
        <source>Copyright:</source>
        <translation>Copyright:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ui/AboutDialogForm.ui" line="91"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TerraView is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TerraView is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You should have received a copy of the GNU Lesser General Public License along with TerraView. &lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Comments, suggestions and feedbacks can be sent to TerraLib Team (&lt;a href=&quot;mailto:gribeiro@dpi.inpe.br&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;terralib-team@dpi.inpe.br&lt;/span&gt;&lt;/a&gt;) at &lt;a href=&quot;http://www.dpi.inpe.br&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Image Processing Division&lt;/span&gt;&lt;/a&gt; (DPI), &lt;a href=&quot;http://www.inpe.br&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Brazilian National Institute of Space Research&lt;/span&gt;&lt;/a&gt; (INPE).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TerraView é um software livre: você pode redistribuí-lo e / ou modificá-lo sob os termos da GNU Lesser General Public License como publicado pela Free Software Foundation, tanto a versão 3 da Licença, ou (a seu critério) qualquer versão posterior.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TerraView é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; mesmo sem a garantia implícita de COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM DETERMINADO FIM. Veja a GNU Lesser General Public License para mais detalhes.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com TerraView.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;
Comentários, sugestões e feedbacks podem ser enviadas para equipe da TerraLib (&lt;a href=&quot;mailto:gribeiro@dpi.inpe.br&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;terralib-team@dpi.inpe.br&lt;/span&gt;&lt;/a&gt;) da &lt;a href=&quot;http://www.dpi.inpe.br&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Divisão de Processamento de Imagem&lt;/span&gt;&lt;/a&gt; (DPI), &lt;a href=&quot;http://www.inpe.br&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Instituto Nacional de Pesquisas Espaciais &lt;/span&gt;&lt;/a&gt; (INPE).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ui/AboutDialogForm.ui" line="132"/>
        <source>TerraView Version:</source>
        <translation>Versão do TerraView:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ui/AboutDialogForm.ui" line="142"/>
        <source>TerraLib Version:</source>
        <translation>Versão da TerraLib:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ui/AboutDialogForm.ui" line="152"/>
        <source>Build Date:</source>
        <translation>Data de Criação:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ui/AboutDialogForm.ui" line="219"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>ProjectInfoDialog</name>
    <message>
        <location filename="../../../src/terraview/ProjectInfoDialog.cpp" line="14"/>
        <source>Project Properties</source>
        <translation>Propriedades do Projeto</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ProjectInfoDialog.cpp" line="18"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ProjectInfoDialog.cpp" line="19"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancela</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ProjectInfoDialog.cpp" line="20"/>
        <source>&amp;Help</source>
        <translation>&amp;Ajuda</translation>
    </message>
</context>
<context>
    <name>ProjectInfoWidgetForm</name>
    <message>
        <location filename="../../../src/terraview/ui/ProjectInfoWidgetForm.ui" line="14"/>
        <source>Project Information</source>
        <translation>Informações do Projeto</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ui/ProjectInfoWidgetForm.ui" line="22"/>
        <source>Title:</source>
        <translation>Título:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ui/ProjectInfoWidgetForm.ui" line="36"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ui/ProjectInfoWidgetForm.ui" line="50"/>
        <source>Location:</source>
        <translation>Localização:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/ui/ProjectInfoWidgetForm.ui" line="74"/>
        <source>Description:</source>
        <translation>Descrição:</translation>
    </message>
</context>
<context>
    <name>ProjectWidget</name>
    <message>
        <location filename="../../../src/terraview/settings/ProjectWidget.cpp" line="15"/>
        <source>Default author for new projects.</source>
        <translation>Autor padrão para novos projetos.</translation>
    </message>
</context>
<context>
    <name>ProjectWidgetForm</name>
    <message>
        <location filename="../../../src/terraview/settings/ui/ProjectWidgetForm.ui" line="26"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/settings/ui/ProjectWidgetForm.ui" line="36"/>
        <source>Author name :</source>
        <translation>Nome do autor :</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/settings/ui/ProjectWidgetForm.ui" line="61"/>
        <source>Maximum number of projects shown in “Recent projects”:</source>
        <translation>Número máximo de projetos apresentados em &quot;Projetos recentes&quot;:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/settings/ui/ProjectWidgetForm.ui" line="103"/>
        <source>Auto select</source>
        <translation>Auto seleção</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/settings/ui/ProjectWidgetForm.ui" line="113"/>
        <source>Last</source>
        <translation>Mais recente</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/settings/ui/ProjectWidgetForm.ui" line="123"/>
        <source>None</source>
        <translation>Nenhum</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../../src/terraview/Project.cpp" line="163"/>
        <source>Could not read project file: </source>
        <translation>Não foi possível ler arquivo de projeto: </translation>
    </message>
    <message>
        <location filename="../../../src/terraview/Project.cpp" line="174"/>
        <source>Could not read project information in the file: </source>
        <translation>Não foi possível ler as informações do projeto no arquivo: </translation>
    </message>
    <message>
        <location filename="../../../src/terraview/Project.cpp" line="180"/>
        <source>Error reading the document </source>
        <translation>Erro ao ler o documento </translation>
    </message>
    <message>
        <location filename="../../../src/terraview/Project.cpp" line="180"/>
        <source>, the start element wasn&apos;t found.</source>
        <translation>, o elemento inicial não foi encontrado.</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/Project.cpp" line="186"/>
        <source>The first tag in the document </source>
        <translation>A primeira tag no documento</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/Project.cpp" line="186"/>
        <source> is not &apos;Project&apos;.</source>
        <translation> não é &apos;Projeto&apos;.</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="241"/>
        <source>Default project</source>
        <translation>Projeto Padrão</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/settings/ProjectWidgetFactory.cpp" line="30"/>
        <source>Project</source>
        <translation>Projeto</translation>
    </message>
</context>
<context>
    <name>TerraView</name>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="346"/>
        <source>Tasks Progress</source>
        <translation>Progresso de tarefas</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="359"/>
        <source>&amp;Customize...</source>
        <translation>&amp;Personalizar...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="359"/>
        <source>Customize the system preferences</source>
        <translation>Personalizar as preferências do sistema</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="360"/>
        <source>&amp;Advanced...</source>
        <translation>&amp;Avançado...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="360"/>
        <source>Exchange data sets between data sources</source>
        <translation>Intercâmbio de dados entre fontes de dados</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="361"/>
        <source>&amp;Layer...</source>
        <translation>&amp;Camada...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="361"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="362"/>
        <source>Exchange data sets from layers</source>
        <translation>Intercâmbio do conjunto de dados da camada</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="362"/>
        <source>&amp;Exchange...</source>
        <translation>&amp;Intercâmbio</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="363"/>
        <source>&amp;Data Source Explorer...</source>
        <translation>Explorador da Fonte de &amp;Dados...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="363"/>
        <source>Show or hide the data source explorer</source>
        <translation>Mostrar ou ocultar o explorador da fonte de dados</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="364"/>
        <source>&amp;Query Data Source...</source>
        <translation>&amp;Consultar Fonte de Dados...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="364"/>
        <source>Allows you to query data in a data source</source>
        <translation>Permite-lhe consultar os dados em uma fonte de dados</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="365"/>
        <source>&amp;Raster Multi Resolution...</source>
        <translation>Matricial (&amp;Raster) Multi Resolução...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="365"/>
        <source>Creates multi resolution over a raster...</source>
        <translation>Cria muiti resolução sobre um matricial (raster)...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="368"/>
        <source>&amp;Manage Plugins...</source>
        <translation>&amp;Gerenciar Plugins...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="368"/>
        <source>Manage the application plugins</source>
        <translation>Gerenciar os plugins do aplicativo</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="371"/>
        <source>&amp;View Help...</source>
        <translation>&amp;Exibir Ajuda...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="371"/>
        <source>Shows help dialog</source>
        <translation>Mostrar janela de ajuda</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="372"/>
        <source>&amp;About...</source>
        <translation>&amp;Sobre...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="375"/>
        <source>&amp;From Data Source...</source>
        <translation>Da &amp;Fonte de Dados...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="375"/>
        <source>Add a new layer from all available data sources</source>
        <translation>Adicione uma nova camada de todas as fontes de dados disponíveis</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="376"/>
        <source>Add &amp;Folder Layer...</source>
        <translation>A&amp;dicionar Pasta de Camadas...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="376"/>
        <source>Add a new folder layer</source>
        <translation>Adiciona uma nova pasta de camadas</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="377"/>
        <source>&amp;Query Dataset...</source>
        <translation>Dados de &amp;Consulta...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="377"/>
        <source>Add a new layer from a queried dataset</source>
        <translation>Adiciona uma nova camada a partir de um conjunto de dados de consulta</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="378"/>
        <source>&amp;Tabular File...</source>
        <translation>Arquivo &amp;Tabular...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="378"/>
        <source>Add a new layer from a Tabular file</source>
        <translation>Adiciona uma nova camada de um arquivo tabular</translation>
    </message>
    <message>
        <source>&amp;Change Layer Data Source</source>
        <translation type="vanished">&amp;Alterar Fonte de Dados da Camada</translation>
    </message>
    <message>
        <source>Chanage layer Data Source</source>
        <translation type="vanished">Alterar Fonte de Dados da Camada</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="379"/>
        <source>&amp;Update Layer Data Source</source>
        <translation>&amp;Atualizar Fonte de Dados da Camada</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="379"/>
        <source>Update layer Data Source</source>
        <translation>Atualizar Fonte de Dados da Camada</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="380"/>
        <source>&amp;Properties...</source>
        <translation>&amp;Propriedades...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="380"/>
        <source>Show the project properties</source>
        <translation>Mostra as propriedades do projeto</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="383"/>
        <source>&amp;Edit Legend...</source>
        <translation>&amp;Editar Legenda...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="384"/>
        <source>&amp;Histogram...</source>
        <translation>&amp;Histograma...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="385"/>
        <source>&amp;Scatter...</source>
        <translation>Gráfico de Dispersão (&amp;Scatter)</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="386"/>
        <source>&amp;Pie/Bar Chart...</source>
        <translation>Gráfico &amp;Pizza/Barra</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="387"/>
        <source>Query...</source>
        <translation>Consulta...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="388"/>
        <source>&amp;Link...</source>
        <translation>&amp;Link...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="389"/>
        <source>&amp;Composition Mode...</source>
        <translation>Mode &amp;Composição...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="389"/>
        <source>Set the composition mode to renderer the selected layer</source>
        <translation>Defina o modo de composição para processar a camada selecionada</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="392"/>
        <source>&amp;New Project...</source>
        <translation>&amp;Novo Projeto...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="393"/>
        <source>&amp;Save Project</source>
        <translation>&amp;Salvar Projeto</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="394"/>
        <source>Save Project &amp;As...</source>
        <translation>Salvar Projeto &amp;Como...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="395"/>
        <source>&amp;Open Project...</source>
        <translation>&amp;Abrir Projeto...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="396"/>
        <source>&amp;Restart System...</source>
        <translation>&amp;Reiniciar Sistema...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="396"/>
        <source>Restart the system.</source>
        <translation>Reiniciar o sistema.</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="397"/>
        <source>E&amp;xit</source>
        <translation>&amp;Sair</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="398"/>
        <source>Print Pre&amp;view...</source>
        <translation>Pré-&amp;visualização de Impressão...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="399"/>
        <source>&amp;Print...</source>
        <translation>&amp;Imprimir...</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="402"/>
        <source>Measure &amp;Distance</source>
        <translation>Medida de &amp;Distância</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="403"/>
        <source>Measure &amp;Area</source>
        <translation>Medida de &amp;Área</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="404"/>
        <source>Measure &amp;Angle</source>
        <translation>Medida de &amp;Ângulo</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="456"/>
        <source>&amp;File</source>
        <translation>&amp;Arquivo</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="459"/>
        <source>Recent &amp;Projects</source>
        <translation>&amp;Projetos Recentes</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="482"/>
        <source>&amp;View</source>
        <translation>&amp;Exibir</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="485"/>
        <source>&amp;Toolbars</source>
        <translation>&amp;Barra de Ferramentas</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="497"/>
        <source>&amp;Project</source>
        <translation>&amp;Projeto</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="500"/>
        <source>&amp;Add Layer</source>
        <translation>Adiciona Camada</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="515"/>
        <source>&amp;Layer</source>
        <translation>&amp;Camada</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="538"/>
        <source>&amp;Map</source>
        <translation>&amp;Mapa</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="577"/>
        <source>&amp;Tools</source>
        <translation>&amp;Ferramentas</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="580"/>
        <source>&amp;Data Exchanger</source>
        <translation>Intercâmbio de &amp;Dados</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="594"/>
        <source>Pl&amp;ugins</source>
        <translation>Pl&amp;ugins</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="601"/>
        <source>&amp;Help</source>
        <translation>A&amp;juda</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="843"/>
        <source>The system will be restarted.</source>
        <translation>O sistema será reiniciado.</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="844"/>
        <source>Do you want to continue?</source>
        <translation>Você deseja continuar ?</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="845"/>
        <source>Restart system</source>
        <translation>Reiniciar sistema</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="861"/>
        <source>Default Project</source>
        <translation>Projeto Padrão</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="877"/>
        <source>Open project file</source>
        <translation>Abrir arquivo de projeto</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="888"/>
        <source>Fail to open project.</source>
        <translation>Falha ao abrir projeto</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="903"/>
        <source>TerraView project(*.</source>
        <translation>Projeto TerraView(*.</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="905"/>
        <source>Save project</source>
        <translation>Salvar projeto</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="945"/>
        <source>Save Project File</source>
        <translation>Salvar Arquivo de Projeto</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="993"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1046"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1103"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1277"/>
        <source>Select a layer in the layer explorer!</source>
        <translation>Selecione uma camada no explorador de camadas !</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1002"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1056"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1112"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1171"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1225"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1289"/>
        <source>There are invalid layers selected!</source>
        <translation>Há camadas inválidas selecionadas !</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1033"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1498"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1537"/>
        <source>Unknown error while trying to add a layer from a queried dataset!</source>
        <translation>Erro desconhecido ao tentar adicionar uma camada de um conjunto de dados de consulta !</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1072"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1128"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1182"/>
        <source>Layer</source>
        <translation>Camada</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1079"/>
        <source>Histogram</source>
        <translation>Histograma</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1136"/>
        <source>Scatter</source>
        <translation>Gráfico de Dispersão</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1161"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1216"/>
        <location filename="../../../src/terraview/TerraView.cpp" line="1723"/>
        <source>Select a single layer in the layer explorer!</source>
        <translation>Selecione uma única camada no explorador de camadas !</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1450"/>
        <source>Unknown error while trying to add a layer from a dataset!</source>
        <translation>Erro desconhecido ao tentar adicionar uma camada de um conjunto de dados !</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1549"/>
        <source>Folder layer name:</source>
        <translation>Nome da pasta de camada:</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1550"/>
        <source>Enter folder layer name</source>
        <translation>Entre com o nome da pasta de camada</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1557"/>
        <source>Enter the layer name!</source>
        <translation>Entre com o nome da camada !</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1570"/>
        <source>There&apos;s no current project!</source>
        <translation>Não há nenhum projeto atual !</translation>
    </message>
    <message>
        <source>Unknown error while trying to change a layer data source!</source>
        <translation type="vanished">Erro desconhecido ao tentar alterar uma fonte de dados da camada !</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1635"/>
        <source>Unknown error while trying to update a layer data source!</source>
        <translation>Erro desconhecido ao tentar atualizar uma fonte de dados da camada !</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1817"/>
        <source>DataSetExplorer Error!</source>
        <translation>Erro no Explorador de Dados !</translation>
    </message>
    <message>
        <location filename="../../../src/terraview/TerraView.cpp" line="1854"/>
        <source>The current project has unsaved changes. Do you want to save them?</source>
        <translation>O projeto atual tem alterações não salvas. Você deseja salvá-las ?</translation>
    </message>
</context>
<context>
    <name>TerraViewController</name>
    <message>
        <location filename="../../../src/terraview/TerraViewController.cpp" line="141"/>
        <source>Error loading the registered projects: %1</source>
        <translation>Erro ao carregar os projetos registrados: %1</translation>
    </message>
</context>
</TS>
